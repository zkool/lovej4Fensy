package com.ketayao.pojo;

import java.sql.Timestamp;
import java.util.List;

import com.ketayao.fensy.db.POJO;

public class Picture extends POJO{

	private static final long serialVersionUID = -4998256044892210924L;
	public static final Picture INSTANCE = new Picture();

	private String title;
	private String description;
	private Integer priority = 99;
	private Timestamp createTime;
	private Long albumId;
	private String path;
	private String link;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public Long getAlbumId() {
		return albumId;
	}
	public void setAlbumId(Long albumId) {
		this.albumId = albumId;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	private static final String PICTUREIDS = "albumId = ? order by priority asc";
	private static final String PICTUREIDS2 = "albumId in (?) order by priority asc";
	
	/**
	 * 获取相册的图片
	 * @param filter
	 * @return
	 */
	public List<Picture> findAlbumPic(long albumId){
		List<Long> ids = ids(PICTUREIDS,albumId);
		List<Picture> list = loadList(ids);
		return list;
	}
	
	/**
	 * @param albumIds
	 * @return
	 */
	public List<Picture> findAlbumPicByIn(String albumIds){
		List<Long> ids = ids(PICTUREIDS2,albumIds);
		List<Picture> list = loadList(ids);
		return list;
	} 
}
