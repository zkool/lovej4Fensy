package com.ketayao.pojo;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ketayao.fensy.db.POJO;
import com.ketayao.system.SystemConfig;
import com.ketayao.util.HttpClientTool;

public class OscUser extends POJO{

	private static final long serialVersionUID = -1550687234928377079L;

	public static final OscUser INSTANCE = new OscUser();
	
	private String oscId;
	private String name;
	private String email;
	private String gender;
	private String avatar;
	private String location;
	private String province;
	private String city;
	private String platforms;
	private String expertise;
	private Timestamp joinTime;
	private String url;
	private String fansCount;
	private String favoriteCount;
	private String followersCount;
	private String accessToken;
	private String refreshToken;
	private String tokenType;
	private int expiresIn = 0;
	private Timestamp createTime;
	private Timestamp updateTime;
	private int flag = 0;
	private String state;
	public String getOscId() {
		return oscId;
	}
	public void setOscId(String oscId) {
		this.oscId = oscId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPlatforms() {
		return platforms;
	}
	public void setPlatforms(String platforms) {
		this.platforms = platforms;
	}
	public String getExpertise() {
		return expertise;
	}
	public void setExpertise(String expertise) {
		this.expertise = expertise;
	}
	public Timestamp getJoinTime() {
		return joinTime;
	}
	public void setJoinTime(Timestamp joinTime) {
		this.joinTime = joinTime;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFansCount() {
		return fansCount;
	}
	public void setFansCount(String fansCount) {
		this.fansCount = fansCount;
	}
	public String getFavoriteCount() {
		return favoriteCount;
	}
	public void setFavoriteCount(String favoriteCount) {
		this.favoriteCount = favoriteCount;
	}
	public String getFollowersCount() {
		return followersCount;
	}
	public void setFollowersCount(String followersCount) {
		this.followersCount = followersCount;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public String getTokenType() {
		return tokenType;
	}
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
	public int getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 获取当前登录用户的账户信息
	 * @param access_token
	 * @return
	 */
	public static OscUser getOscUser(String access_token){
		String url = SystemConfig.getConfig().get("osc.app.url") + "/action/openapi/user";
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", access_token);
		params.put("dataType", "json");
		try {
			OscUser user = new OscUser();
			String info = HttpClientTool.sendHttpPost(url, params);
			if(info != null && !"".equals(info) && !"failure".equals(info)){
				JSONObject obj = JSONObject.parseObject(info);
				if(obj.containsKey("id")){
					user.setOscId(obj.getString("id"));
				}
				if(obj.containsKey("email")){
					user.setEmail(obj.getString("email"));
				}
				if(obj.containsKey("name")){
					user.setName(obj.getString("name"));
				}
				if(obj.containsKey("gender")){
					user.setGender(obj.getString("gender"));
				}
				if(obj.containsKey("avatar")){
					user.setAvatar(obj.getString("avatar"));
				}
				if(obj.containsKey("location")){
					user.setLocation(obj.getString("location"));
				}
				if(obj.containsKey("url")){
					user.setUrl(obj.getString("url"));
				}
				
				return user;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 个人主页详情
	 * @param access_token
	 * @return
	 */
	public static OscUser getMyInformation(String access_token){
		String url = SystemConfig.getConfig().get("osc.app.url") + "/action/openapi/my_information";
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", access_token);
		params.put("dataType", "json");
		try {
			OscUser user = new OscUser();
			String info = HttpClientTool.sendHttpPost(url, params);
			if(info != null && !"".equals(info) && !"failure".equals(info)){
				JSONObject obj = JSONObject.parseObject(info);
				if(obj.containsKey("user")){
					user.setOscId(obj.getLong("user")+"");
				}
				if(obj.containsKey("name")){
					user.setName(obj.getString("name"));
				}
				if(obj.containsKey("province")){
					user.setProvince(obj.getString("province"));
				}
				if(obj.containsKey("city")){
					user.setCity(obj.getString("city"));
				}
				if(obj.containsKey("platforms")){
					user.setPlatforms(obj.getString("platforms"));
				}
				if(obj.containsKey("expertise")){
					user.setExpertise(obj.getString("expertise"));
				}
				if(obj.containsKey("joinTime")){
					String joinTime = obj.getString("joinTime");
					if(joinTime != null && !"".equals(joinTime)){
						user.setJoinTime(Timestamp.valueOf(joinTime));
					}
				}
				if(obj.containsKey("fansCount")){
					user.setFansCount(obj.getString("fansCount"));
				}
				if(obj.containsKey("favoriteCount")){
					user.setFavoriteCount(obj.getString("favoriteCount"));
				}
				if(obj.containsKey("followersCount")){
					user.setFollowersCount(obj.getString("followersCount"));
				}
				
				return user;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
