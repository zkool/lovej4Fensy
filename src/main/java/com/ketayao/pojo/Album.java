package com.ketayao.pojo;

import java.sql.Timestamp;
import java.util.List;

import com.ketayao.fensy.db.POJO;

public class Album extends POJO{

	private static final long serialVersionUID = -366950742129541153L;
	public static final Album INSTANCE = new Album();

	private String name;
	private String description;
	private Integer priority = 99;
	private Timestamp createTime;
	private String type;
	
	private List<Picture> pictures;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Picture> getPictures() {
		return pictures;
	}
	public void setPictures(List<Picture> pictures) {
		this.pictures = pictures;
	}
	
	private static final String ALUBMIDS = "type = ? order by priority asc";
	
	/**
	 * 获取相册信息
	 * @param type
	 * @return
	 */
	public List<Album> findAlbum(String type){
		List<Long> ids = ids(ALUBMIDS,type);
		List<Album> list = loadList(ids);
		return list;
	}
}
