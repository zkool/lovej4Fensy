package com.ketayao.util;

import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

public class HttpClientTool {
	
	/**
	 * 发送POST请求
	 * @param url
	 * @param valueList
	 * @return
	 * @throws Exception
	 */
	public static String sendHttpPost(String url, Map<String, String> valueList) throws Exception {
        HttpClient client = null;
        PostMethod method = null;
        NameValuePair[] valuePair = null;
        try {
            if (StringUtils.isEmpty(url))
            	return "failure";
            if (valueList == null || valueList.size() == 0)
            	return "failure";

            client = new HttpClient();
          //User-Agent 访问OSC openAPI 需加这个参数 否则服务器返回403
    		client.getParams().setParameter(HttpMethodParams.USER_AGENT,
    				"Mozilla/5.0 (X11; U; Linux i686; zh-CN; rv:1.9.1.2) Gecko/20090803");
            method = new PostMethod(url);
            valuePair = new NameValuePair[valueList.size()];
            
            
            int i=0;
            for(Map.Entry<String, String> entry : valueList.entrySet())
                valuePair[i++] = new NameValuePair(entry.getKey(), entry.getValue());
            
            method.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
            method.setRequestBody(valuePair);
            client.executeMethod(method);
            return method.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            method.releaseConnection();
        }
    }
    
    /**
     * 发送GET请求
     * @param url
     * @param valueList
     * @return
     * @throws Exception
     */
    public static String sendHttpGet(String url, Map<String, String> valueList) throws Exception {
        HttpClient client = null;
        GetMethod method = null;
        NameValuePair[] valuePair = null;
        try {
            if (StringUtils.isEmpty(url))
            	return "failure";
            if (valueList == null || valueList.size() == 0)
            	return "failure";

            client = new HttpClient();
          //User-Agent 
            client.getParams().setParameter(HttpMethodParams.USER_AGENT,
                    		"Mozilla/5.0 (X11; U; Linux i686; zh-CN; rv:1.9.1.2) Gecko/20090803");

            method = new GetMethod(url);
            valuePair = new NameValuePair[valueList.size()];
            
            int i=0;
            for(Map.Entry<String, String> entry : valueList.entrySet())
                valuePair[i++] = new NameValuePair(entry.getKey(), entry.getValue());
            method.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
            method.setQueryString(valuePair);
            client.executeMethod(method);
            return method.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            method.releaseConnection();
        }
    } 
}