package com.ketayao.action;

import java.util.ArrayList;
import java.util.List;

import com.ketayao.fensy.mvc.RequestContext;
import com.ketayao.pojo.Album;
import com.ketayao.pojo.Category;
import com.ketayao.pojo.Picture;

/** 
 * @description: 关于我们
 * @version 1.0
 * @author Kool Zhao
 * @createDate 2014-4-6;下午11:19:22
 */
public class AboutUsAction {
	
	protected static final String READ = "blog/about-us";

	public String index(RequestContext rc){
		List<Long> ids = Album.INSTANCE.ids("type = ?","team");
		List<Picture> list = new ArrayList<Picture>();
		if(ids != null && ids.size() > 0){
			StringBuilder sql = new StringBuilder();
			for (int i = 0; i < ids.size(); i++) {
				if(i != 0 ){
					sql.append(',');
				}
				sql.append(ids.get(i));
					
			}
			list = Picture.INSTANCE.findAlbumPicByIn(sql.toString());
			
		}
		rc.setRequestAttr("pictures", list);
		List<Category> categories = Category.INSTANCE.findTree(false);
		rc.setRequestAttr("categories", categories);
		
		return READ;
	}
}
