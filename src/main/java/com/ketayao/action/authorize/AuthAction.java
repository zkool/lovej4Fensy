package com.ketayao.action.authorize;

import com.ketayao.fensy.mvc.RequestContext;
import com.ketayao.system.SystemConfig;

public class AuthAction {

	private static final String VIEW = "auth/auth";
	
	public String index(RequestContext rc){
		String outhUrl = SystemConfig.getConfig().get("osc.app.url") + SystemConfig.getConfig().get("osc.app.oauth2.authorize")+"?response_type=code&client_id=" + SystemConfig.getConfig().get("osc.app.id") + "&redirect_uri=" + SystemConfig.getConfig().get("osc.app.callback");
		rc.setRequestAttr("outhUrl", outhUrl);
		return VIEW;
	}
}
