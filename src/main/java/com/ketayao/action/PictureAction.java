package com.ketayao.action;

import java.util.List;

import com.ketayao.fensy.mvc.RequestContext;
import com.ketayao.pojo.Album;
import com.ketayao.pojo.Picture;

public class PictureAction {

	protected static final String READ = "blog/album";
	
	public String index(RequestContext rc){
		List<Album> albums =  Album.INSTANCE.findAlbum("common");
		if(albums != null & albums.size() > 0){
			for(Album a:albums){
				a.setPictures(Picture.INSTANCE.findAlbumPic(a.getId()));
			}
		}
		rc.getRequest().setAttribute("albums", albums);
		return READ;
	}
}
