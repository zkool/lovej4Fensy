<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>KOOL Love Story</title>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link href="${rc.contextPath}/styles/520/default.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="${rc.contextPath}/styles/520/jquery.js"></script>
<script type="text/javascript" src="${rc.contextPath}/styles/520/garden.js"></script>
<script type="text/javascript" src="${rc.contextPath}/styles/520/functions.js"></script>
</head>

<body>
 <div id="mainDiv">
  <div id="content">
   <div id="code">
    <p><span class="comments">Dear WRF：</span><br />
       <span class="comments">　　2014.03.16，我们在一起的第一天</span><br />
       <span class="comments">　　我深深的被你那句“尽管你一无所有，我也会和你在一起的”感动了</span><br />
       <span class="comments">　　尽管你说出的话很天真，但我却感觉到你的语气和眼神感觉到了你的纯真可爱</span><br />
       Boy =<span class="string">"赵奎"</span>;<br />
       Girl=<span class="string">"王瑞芬"</span>;<br />
	   <br />
	   Preface:<br />
       <span class="comments">　　2014, 爱你一世，我会用我的行动来弥补那错过的2013</span><br />
	   <br />
       Warn:<br />
       <span class="comments">　　从朋友到情侣，我们注定已经没有回头的路</span><br />
       <span class="comments">　　一回头，我们便会成了那“最熟悉的陌生人”</span><br />
       <br />
       Future:<br />
       <span class="comments">　　未来，很远，很近，很深，很难测</span><br />
       <span class="comments">　　但不管未来怎样，我都会用最自信，最美好的心态去对待你和我们的爱情</span><br />
       <span class="comments">　　未来，何去何从？</span><br />
       <span class="comments">　　你的“未来我会和你一直走下去”这句话给了我一盏引路灯</span><br />
	   <br />
       Saying:<br />
       <span class="comments">　　甜言蜜语我不会说，金钱地位我也没有</span><br />
       <span class="comments">　　但我会去努力，会去拼搏，会去创造一切你需要的，你梦想的</span><br />
       <br />
       <br />
       The and<br />
       <span class="comments">　　记住你每一句对我说过的话</span><br />
       <span class="comments">　　我很爱你，但愿你对我说过的每一句话都能成真</span></br />
	   <span class="comments">　　让我们一起续写未来</span></br />
	   <span class="comments">　　Let us write the future</span></br />
    </p>
   </div>
   <div id="loveHeart">
   <canvas id="garden"></canvas>
   <div id="words">
   <div id="messages">
<center>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3712320065678109";
/* lovead */
google_ad_slot = "0650322805";
google_ad_width = 320;
google_ad_height = 50;
//-->
</script>
</script>
</center>　　　　王瑞芬，我赵奎爱你一生一世
</div>
              </div>
          </div>
      </div>
        </div>
        <div id="copyright">
             
        <object type="application/x-shockwave-flash" data="${rc.contextPath}/styles/dewplayer/dewplayer-mini.swf" id="dewplayer" name="dewplayer" width="200" height="20">
			<param name="wmode" value="transparent" />
			<param name="movie" value="dewplayer-mini.swf" />
			<param name="flashvars" value="mp3=${rc.contextPath}/styles/520/LOVEPJY.mp3&autoplay=1&autoreplay=1" />
		</object><br /> 
		    作者: <a href="http://www.zhaokuikui.cn/">ZKOOL</a>
        </div>
    </div>
    <script type="text/javascript">
        var offsetX = $("#loveHeart").width() / 2;
        var offsetY = $("#loveHeart").height() / 2 - 55;
        
        if (!document.createElement('canvas').getContext) {
            var msg = document.createElement("div");
            msg.id = "errorMsg";
            msg.innerHTML = "Your br /owser doesn't support HTML5!<br />Recommend use Chrome 14+/IE 9+/Firefox 7+/Safari 4+"; 
            document.body.appendChild(msg);
            $("#code").css("display", "none")
            $("#copyright").css("position", "absolute");
            $("#copyright").css("bottom", "10px");
            document.execCommand("stop");
        } else {
            setTimeout(function () {
                adjustWordsPosition();
                startHeartAnimation();
            }, 10000);
            
            $("#accept").click(function(){
                $(this).hide();
                $("#elapseClock").show();
                var together = new Date();
                timeElapse(together);
                setInterval(function () {
                    timeElapse(together);
                }, 500);
            })
            adjustCodePosition();
            $("#code").typewriter();
        }
    </script>
    
</body>
</html>
