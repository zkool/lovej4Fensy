<#compress>
<#import "WEB-INF/www/admin/lib/common.ftl" as com>
<#include "WEB-INF/www/admin/lib/album-nav.ftl"/>

<#--currentNav定义-->
<#assign currentNav>${bundle("site.albumManage.add")}</#assign>

<#escape x as x?html>
<@com.page title=title sideNav=sideNav sideNavUrl=sideNavUrl parentNav=parentNav parentNavUrl=parentNavUrl currentNav=currentNav>

<#-- form验证 -->
<link rel="stylesheet" href="${rc.contextPath}/styles/formValidator.2.2.1/css/validationEngine.jquery.css" type="text/css"/>
<script src="${rc.contextPath}/styles/formValidator.2.2.1/js/languages/jquery.validationEngine-${(rc.locale)!'zh_CN'}.js" type="text/javascript" charset="utf-8"></script>
<script src="${rc.contextPath}/styles/formValidator.2.2.1/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
    jQuery(document).ready(function(){
        // binds form submission and fields to the validation engine
        jQuery("#formID").validationEngine();
    });
    
</script>


<div id="main">
	<form action="${rc.contextPath}/admin/album/create" class="jNice" method="POST" id="formID">
		<h3 class="lovej-action">
			<#if success??>${bundle("action.album.add")}</#if>
		</h3>
		<h3>
			<#noescape>${bundle("form.notice.required")}</#noescape>
		</h3>
		<h3>
			${bundle("form.notice.priority")}
		</h3>
		<fieldset>
			<p>
				<label>${bundle("album.name")}<font color="red">*</font>:</label><input type="text" name="name" class="validate[required,maxSize[50]] text-long"  id="name"/>
			</p>
			<p>
				<label>${bundle("category.priority")}:</label><input type="text" name="priority" class="validate[optional,custom[integer],maxSize[2]] text-long" value="99" id="priority"/>
			</p>
			<p>
				<label>${bundle("album.type")}:</label>
				<input type="radio" id="type1" name="type"  value="common" checked="checked" />${bundle("album.type.common")}&nbsp;&nbsp;
				<input type="radio" id="type2" name="type"  value="team" />${bundle("album.type.team")}
			</p>
			<p>
				<label>${bundle("album.description")}:</label><textarea rows="1" cols="1" name="description" id="description" class="validate[optional,maxSize[255]]"></textarea>
			</p>
			<input id="actionAlbum" type="submit" value='${bundle("form.save")}' />
		</fieldset>
	</form>
</div>
<!-- // #main -->

</@com.page>

</#escape>
</#compress>