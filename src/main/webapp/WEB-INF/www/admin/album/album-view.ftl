<#compress>
<#import "WEB-INF/www/admin/lib/view.ftl" as view>

<#escape x as x?html>
<@view.page>

<style type="text/css">
#wrapper {width:300px;}
#main  {width:290px;float:left;}
#main label {font-size: 12px;font-weight: bold;}
#main span {font-size: 12px;color:#c66653;font-weight:normal; }
</style>

<div id="main">
	<h3 class="lovej-action">
		${bundle("action.album.view")}
	</h3>
	<fieldset>
		<p>
			<label>${bundle("album.name")}<font color="red">*</font>:<span>${album.name}</span></label>
		</p>
		<p>
			<label>${bundle("category.priority")}:<span>${album.priority}</span></label>
		</p>
		<p>
			<label>${bundle("album.type")}:<span>${album.type}</span></label>
		</p>
		<p>
			<label>${bundle("category.createTime")}:<span>${album.createTime?string('yyyy-MM-dd HH:mm:ss')}</span></label>
		</p>
		<p>
			<label>${bundle("album.description")}:</label><span>${album.description}</span>
		</p>
	</fieldset>
</div>
<!-- // #main -->

</@view.page>

</#escape>
</#compress>