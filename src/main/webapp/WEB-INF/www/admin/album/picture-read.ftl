<#compress>
<#import "WEB-INF/www/admin/lib/common.ftl" as com>
<#include "WEB-INF/www/admin/lib/album-nav.ftl"/>

<#--currentNav定义-->
<#assign currentNav>${bundle("site.pictureManage.manage")}</#assign>

<#escape x as x?html>
<@com.page title=title sideNav=sideNav sideNavUrl=sideNavUrl parentNav=parentNav parentNavUrl=parentNavUrl currentNav=currentNav>

<style>
  .mypic ul{ width:100%;}   
  .mypic li{ width:100px; float:left; display:block; height:135px;line-height:20px;margin:8px;} 

</style>

<div id="main">
		<h3 class="lovej-action">
		</h3>
		<h3>
			${bundle("form.notice.priority")}
		</h3>
		
		<div class="mypic">
			<ul>
				<#list albums as a>
					<li>
						<a  href="${rc.contextPath}/admin/picture/view/${a.id}"><img style="width:100px;height:100px;" src="${rc.contextPath}/styles/blog/images/wrf.jpg"/></a>
						<span>${a.name}</span>
					</li>
				</#list>
			</ul>
		</div>

		
</div>
<!-- // #main -->

</@com.page>

</#escape>
</#compress>