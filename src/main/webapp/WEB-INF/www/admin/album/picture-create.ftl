<#compress>
<#import "WEB-INF/www/admin/lib/common.ftl" as com>
<#include "WEB-INF/www/admin/lib/album-nav.ftl"/>

<#--currentNav定义-->
<#assign currentNav>${bundle("site.pictureManage.add")}</#assign>

<#escape x as x?html>
<@com.page title=title sideNav=sideNav sideNavUrl=sideNavUrl parentNav=parentNav parentNavUrl=parentNavUrl currentNav=currentNav>

<#-- form验证 -->
<link rel="stylesheet" href="${rc.contextPath}/styles/formValidator.2.2.1/css/validationEngine.jquery.css" type="text/css"/>
<script src="${rc.contextPath}/styles/formValidator.2.2.1/js/languages/jquery.validationEngine-${(rc.locale)!'zh_CN'}.js" type="text/javascript" charset="utf-8"></script>
<script src="${rc.contextPath}/styles/formValidator.2.2.1/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>

<script charset="utf-8" src="${rc.contextPath}/styles/kindeditor-4.1.7/kindeditor.js"></script>
<script charset="utf-8" src="${rc.contextPath}/styles/kindeditor-4.1.7/lang/zh_CN.js"></script>
<link rel="stylesheet" type="text/css" href="${rc.contextPath}/styles/kindeditor-4.1.7/themes/default/default.css" />

<script charset="utf-8" src="${rc.contextPath}/styles/admin/js/json2.js"></script>

<script>
    jQuery(document).ready(function(){
        // binds form submission and fields to the validation engine
        jQuery("#formID").validationEngine();
    });
    
    
		KindEditor.ready(function(K) {
			var editor = K.editor({
				uploadJson : '${rc.contextPath}/admin/file/uploads',
				cssPath : ['${rc.contextPath}/styles/kindeditor-4.1.7/plugins/shcode/shcode.css'],
				allowFileManager : true
			});
			K('#J_selectImage').click(function() {
				editor.loadPlugin('multiimage', function() {
					editor.plugin.multiImageDialog({
						clickFn : function(urlList) {
							var div = K('#J_imageView');
							div.html('');
							K.each(urlList, function(i, data) {
								//var jsonStr = JSON.stringify(data);
								//alert(jsonStr)
								var html = '<fieldset>' +
												'<div id="container_picture">' +
													'<div id="photo"><img style="width:200px;height:200px;" src="' + data.url + '" id="img_' + i + '"/></div>' +
													'<div id="content">' +
														'<p>' +
															'<label>${bundle("picture.title")}<font color="red">*</font>:</label><input type="text" class="validate[required,maxSize[50]] text-long" value="' + data.title + '" id="title_' + i + '"/>' +
														'</p>' +
														'<p>' +
															'<label>${bundle("category.priority")}:</label><input type="text" name="priority" class="validate[optional,custom[integer],maxSize[2]] text-long" value="' + i +'" id="priority_' + i + '"/>' +
														'</p>' +
														'<p>' +
															'<label>${bundle("picture.description")}:</label><textarea rows="1" cols="1" style="height:50px;" id="description_' + i + '" class="validate[optional,maxSize[255]]"></textarea>' +
														'</p>' +
													'</div>' +
												'</div> ' +
											'</fieldset>';
								div.append(html);
							});
							editor.hideDialog();
						}
					});
				});
			});
		});
		
		function subForm(){
			var len = $("img[id^='img_']").length;
			if(len == 0){
				alert("请先上传图片");
				return false;
			}
			
			var jsonStr = "";
			$("img[id^='img_']").each(function(){
				var path = $(this).attr("src");
				var flag = $(this).attr("id").split("_")[1];
				var title = $("#title_" + flag).val();
				var priority = $("#priority_" + flag).val();
				var description = $("#description_" + flag).val();
				jsonStr += "{\"path\":\"" + path +"\",\"title\":\"" + title + "\",\"priority\":\"" + priority + "\",\"description\":\"" + description + "\"},";
			})
			
			if(jsonStr.length > 1){
   				jsonStr = jsonStr.substring(0,jsonStr.length - 1);
   			}
    		jsonStr = "[" + jsonStr +"]";
			$("#picture_info").val(jsonStr);
			return true;
		}
</script>

<style>
#container_picture {width: 100%;}
#photo {float: left; width: 40%;text-align:center;}
#content {float: right; width: 60%;}
</style>

<div id="main">
	<form action="${rc.contextPath}/admin/picture/create" class="jNice" method="POST" id="formID" onsubmit="return subForm();">
		<h3 class="lovej-action">
			<#if success??>${bundle("action.picture.add")}</#if>
		</h3>
		<h3>
			<#noescape>${bundle("form.notice.required")}</#noescape>
		</h3>
		<h3>
			${bundle("form.notice.priority")}
		</h3>
		
		<fieldset>
			<p>
				<label>${bundle("album.name")}:</label>
				<select name="albumId" id="albumId">
					<#list albums as p>
						<option value="${p.id}">${p.name}</option>
					</#list>
				</select>
				<input type="hidden" id="picture_info" name="pictureInfo"/>
			</p>
			<input type="button" id="J_selectImage" value="批量上传" />
		</fieldset>
		
		<div id="J_imageView">
		<!--	
		<fieldset>
			<div id="container_picture">
				<div id="photo"><img style="width:200px;height:200px;" id="img_0" src="${rc.contextPath}/styles/blog/images/wrf.jpg"/></div>
				<div id="content">
					<p>
						<label>${bundle("picture.title")}<font color="red">*</font>:</label><input type="text" class="validate[required,maxSize[50]] text-long"  id="title_0"/>
					</p>
					<p>
						<label>${bundle("category.priority")}:</label><input type="text" name="priority" class="validate[optional,custom[integer],maxSize[2]] text-long" value="99" id="priority_0"/>
					</p>
					<p>
						<label>${bundle("picture.description")}:</label><textarea rows="1" cols="1" style="height:50px;" name="description" id="description_0" class="validate[optional,maxSize[255]]"></textarea>
					</p>
				</div>
			</div> 
		</fieldset>
	-->
		
		</div>
		
		<fieldset>
			<input id="actionPicture" type="submit" value='${bundle("form.save")}' />
		</fieldset>
	</form>
</div>
<!-- // #main -->

</@com.page>

</#escape>
</#compress>