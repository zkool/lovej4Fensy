<#--sideNav组合-->
<#assign sideNav0>${bundle("site.albumManage.add")}</#assign>
<#assign sideNav1>${bundle("site.albumManage.manage")}</#assign>
<#assign sideNav2>${bundle("site.pictureManage.add")}</#assign>
<#assign sideNav3>${bundle("site.pictureManage.manage")}</#assign>

<#--sideNavUrl-->
<#assign sideNavUrl0>${rc.contextPath}/admin/album/preCreate</#assign>
<#assign sideNavUrl1>${rc.contextPath}/admin/album/read</#assign>
<#assign sideNavUrl2>${rc.contextPath}/admin/picture/preCreate</#assign>
<#assign sideNavUrl3>${rc.contextPath}/admin/picture/read</#assign>

<#--title定义-->
<#assign title>${bundle("site.albumManage")}</#assign>

<#--parentNav定义-->
<#assign parentNav>${bundle("site.albumManage")}</#assign>

<#--parentNavUrl定义-->
<#assign parentNavUrl>${rc.contextPath}/admin/album/preCreate</#assign>

<#assign sideNav=["${sideNav0}","${sideNav1}","${sideNav2}","${sideNav3}"]> 
<#assign sideNavUrl=["${sideNavUrl0}","${sideNavUrl1}","${sideNavUrl2}","${sideNavUrl3}"]> 