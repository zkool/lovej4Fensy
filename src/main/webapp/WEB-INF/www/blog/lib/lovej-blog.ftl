<#macro menu_nav title index>
  <li <#if title==index[0]>class="current_page_item"<#else>class="page_item"</#if>><a href="${rc.contextPath}/">${index[0]}</a></li>
  	<#list categories as c>
		<li <#if title==c.name>class="current_page_item"<#else>class="page_item"</#if>><a href="${rc.contextPath}/archive/expages/${c.id}">${c.name}</a></li>
	</#list>
  <li <#if title==index[2]>class="current_page_item"<#else>class="page_item"</#if>><a target="_blank" href="${rc.contextPath}/picture">${index[2]}</a></li>
  <li <#if title==index[3]>class="current_page_item"<#else>class="page_item"</#if>><a href="${rc.contextPath}/aboutUs">${index[3]}</a></li>
    <li <#if title==index[1]>class="current_page_item"<#else>class="page_item"</#if>><a href="${rc.contextPath}/contact">${index[1]}</a></li>
    <li <#if title==index[2]>class="current_page_item"<#else>class="page_item"</#if>><a target="_blank" href="${rc.contextPath}/love"><span style="color:red;font-weight:bold;">${index[4]}</span><img src="${rc.contextPath}/styles/blog/images/hot.gif"/></a></li>
</#macro>