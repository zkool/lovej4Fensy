<#import "WEB-INF/www/blog/lib/lovej-blog.ftl" as lj>
<#include "WEB-INF/www/blog/lib/param.ftl"/>
<#assign navTitle>关于我们</#assign>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="zh-CN">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="zh-CN">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="zh-CN">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<meta name="robots" content="index, follow" />
<meta name="Keywords" content="${siteConfig.name} ${keywords}"/>
<meta name="Description" content="${siteConfig.name}"/>
<title>${title} | ${siteConfig.name}</title>
<!--[if lt IE 9]>
<script src="${rc.contextPath}/styles/blog/js/html5.js" type="text/javascript"></script>
<![endif]-->
<link rel="canonical" href="${siteConfig.url}/">
<link rel="shortcut icon" href="${rc.contextPath}/styles/admin/images/favicon.ico" />
<link rel="stylesheet" id="twentytwelve-fonts-css" href="${rc.contextPath}/styles/blog/css/css.css" type="text/css" media="all">
<link rel="stylesheet" id="twentytwelve-style-css" href="${rc.contextPath}/styles/blog/css/style.css" type="text/css" media="all">
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentytwelve-ie-css'  href='${rc.contextPath}/styles/blog/css/ie.css' type='text/css' media='all' />
<![endif]-->

<link rel="stylesheet" id="wp-pagenavi-style-css" href="${rc.contextPath}/styles/blog/css/white_blue.css" type="text/css" media="all">
<link rel="stylesheet" href="${rc.contextPath}/styles/blog/css/tab.css" type="text/css">

<link rel="stylesheet" id="dynamic-top-css" href="${rc.contextPath}/styles/blog/css/dynamic.css" type="text/css" media="all">
<script type="text/javascript" src="${rc.contextPath}/styles/blog/js/jquery1.7-min.js"></script>
<script type="text/javascript" src="${rc.contextPath}/styles/blog/js/tab.js"></script>

</head>

<body class="home blog custom-background custom-font-enabled single-author">
<div id="page" class="hfeed site">
  <header id="masthead" class="site-header" role="banner">
    <hgroup>
      <h1 class="site-title"><a href="${siteConfig.url}" title="${siteConfig.name}" rel="home">${siteConfig.name}</a></h1>
      <h2 class="site-description">${siteConfig.about}</h2>
    </hgroup>
    <nav id="site-navigation" class="main-navigation" role="navigation">
      <h3 class="menu-toggle">菜单</h3>
      <a class="assistive-text" href="#content" title="跳至内容">跳至内容</a>
      <div class="nav-menu">
        <ul>
          <@lj.menu_nav title=navTitle index=index/>
        </ul>
      </div>
    </nav>
    <!-- #site-navigation --> 
    
  </header>
  <!-- #masthead -->
  
       	<div class="tab">

			<div class="tab_pic">
				<ul>
					<#list pictures as p>
						<li><a href="#"><img src="${rc.contextPath}${p.path}" width="225" height="283" /></a></li>
					</#list>
				</ul>
				<span class="tab_san"><img src="${rc.contextPath}/styles/blog/images/tab_san.png" width="17" height="34" /></span>
				<span class="btn_ct"><a class="next"></a><a class="prev"></a></span>
				<span class="btn_bg"></span>
			</div>
			
			<div class="tab_txt">
				<ul>
					<#list pictures as p>
						<li>
							<h3>${p.title}</h3>
							<p>${p.description}</p>
						</li>
					</#list>
				</ul>
			</div>
		
		</div>
       	
  <!-- #main .wrapper -->
  <footer id="colophon" role="contentinfo">
    <div class="site-info"> <a href="${siteConfig.url}" title="${siteConfig.name}">© Copyright 2011-2013 &nbsp;${siteConfig.icp}&nbsp;<a href="${siteConfig.url}">${siteConfig.name}</a></a> </div>
    <!-- .site-info --> 
  </footer>
  <!-- #colophon --> 
</div>
<!-- #page --> 

</body>
</html>