/*
Navicat MySQL Data Transfer

Source Server         : local_mysql
Source Server Version : 50515
Source Host           : 127.0.0.1:3306
Source Database       : fensydb

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2013-08-31 16:49:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lovej_articles
-- ----------------------------
DROP TABLE IF EXISTS `lovej_articles`;
CREATE TABLE `lovej_articles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `postTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `quote` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `summary` text,
  `title` varchar(255) NOT NULL,
  `view` int(11) NOT NULL,
  `categoryId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `trash` tinyint(1) DEFAULT NULL,
  `topTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `lovej_articles_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `lovej_users` (`id`),
  CONSTRAINT `lovej_articles_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `lovej_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for lovej_article_tags
-- ----------------------------
DROP TABLE IF EXISTS `lovej_article_tags`;
CREATE TABLE `lovej_article_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `articleId` bigint(20) NOT NULL,
  `tagId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `articleId` (`articleId`),
  KEY `tagId` (`tagId`),
  CONSTRAINT `lovej_article_tags_ibfk_1` FOREIGN KEY (`articleId`) REFERENCES `lovej_articles` (`id`),
  CONSTRAINT `lovej_article_tags_ibfk_2` FOREIGN KEY (`tagId`) REFERENCES `lovej_tags` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for lovej_attaches
-- ----------------------------
DROP TABLE IF EXISTS `lovej_attaches`;
CREATE TABLE `lovej_attaches` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `articleId` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `download` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `articleId` (`articleId`),
  CONSTRAINT `lovej_attaches_ibfk_1` FOREIGN KEY (`articleId`) REFERENCES `lovej_articles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for lovej_categories
-- ----------------------------
DROP TABLE IF EXISTS `lovej_categories`;
CREATE TABLE `lovej_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `priority` int(11) NOT NULL,
  `trash` tinyint(1) NOT NULL,
  `type` varchar(10) NOT NULL,
  `parentId` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parentId` (`parentId`),
  CONSTRAINT `lovej_categories_ibfk_1` FOREIGN KEY (`parentId`) REFERENCES `lovej_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for lovej_comments
-- ----------------------------
DROP TABLE IF EXISTS `lovej_comments`;
CREATE TABLE `lovej_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `postIP` varchar(20) NOT NULL,
  `postTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `site` varchar(255) DEFAULT NULL,
  `articleId` bigint(20) NOT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `articleId` (`articleId`),
  KEY `parentId` (`parentId`),
  CONSTRAINT `lovej_comments_ibfk_1` FOREIGN KEY (`articleId`) REFERENCES `lovej_articles` (`id`),
  CONSTRAINT `lovej_comments_ibfk_2` FOREIGN KEY (`parentId`) REFERENCES `lovej_comments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for lovej_contacts
-- ----------------------------
DROP TABLE IF EXISTS `lovej_contacts`;
CREATE TABLE `lovej_contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `postIP` varchar(20) NOT NULL,
  `postTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `site` varchar(255) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `trash` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for lovej_links
-- ----------------------------
DROP TABLE IF EXISTS `lovej_links`;
CREATE TABLE `lovej_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `site` varchar(255) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `trash` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for lovej_site_configs
-- ----------------------------
DROP TABLE IF EXISTS `lovej_site_configs`;
CREATE TABLE `lovej_site_configs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `about` varchar(255) NOT NULL,
  `contactDescription` longtext,
  `icp` varchar(50) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for lovej_tags
-- ----------------------------
DROP TABLE IF EXISTS `lovej_tags`;
CREATE TABLE `lovej_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for lovej_users
-- ----------------------------
DROP TABLE IF EXISTS `lovej_users`;
CREATE TABLE `lovej_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `nickname` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `salt` varchar(16) DEFAULT NULL,
  `frozen` tinyint(1) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `role` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `lovej_albums` */

DROP TABLE IF EXISTS `lovej_albums`;

CREATE TABLE `lovej_albums` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '相册',
  `name` varchar(50) DEFAULT NULL COMMENT '相册名称',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `priority` int(11) DEFAULT NULL COMMENT '排序',
  `type` varchar(100) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Table structure for table `lovej_osc_users` */

DROP TABLE IF EXISTS `lovej_osc_users`;

CREATE TABLE `lovej_osc_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'osc用户用户信息',
  `oscId` varchar(200) DEFAULT NULL COMMENT 'osc授权用户ID',
  `name` varchar(200) DEFAULT NULL COMMENT '用户名称',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `gender` varchar(20) DEFAULT NULL COMMENT '性别',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `location` varchar(255) DEFAULT NULL COMMENT '地点',
  `province` varchar(255) DEFAULT NULL COMMENT '省',
  `city` varchar(255) DEFAULT NULL COMMENT '市',
  `platforms` varchar(255) DEFAULT NULL COMMENT '开发平台',
  `expertise` varchar(255) DEFAULT NULL COMMENT '专长领域',
  `joinTime` timestamp NULL DEFAULT NULL COMMENT '加入时间',
  `url` varchar(255) DEFAULT NULL COMMENT '主页',
  `fansCount` varchar(20) DEFAULT NULL COMMENT '粉丝数',
  `favoriteCount` varchar(20) DEFAULT NULL COMMENT '收藏数',
  `followersCount` varchar(20) DEFAULT NULL COMMENT '关注数',
  `accessToken` varchar(255) DEFAULT NULL COMMENT 'access_token值',
  `refreshToken` varchar(255) DEFAULT NULL COMMENT 'refresh_token值',
  `tokenType` varchar(255) DEFAULT NULL COMMENT 'access_token类型',
  `expiresIn` int(11) DEFAULT NULL COMMENT '超时时间(单位秒)',
  `createTime` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` timestamp NULL DEFAULT NULL COMMENT '授权时间',
  `flag` int(2) DEFAULT NULL COMMENT '标识',
  `state` varchar(20) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `lovej_pictures` */

DROP TABLE IF EXISTS `lovej_pictures`;

CREATE TABLE `lovej_pictures` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '照片',
  `albumId` bigint(20) DEFAULT NULL COMMENT '相册ID',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  `link` varchar(255) DEFAULT NULL COMMENT '链接',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `albumId` (`albumId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
